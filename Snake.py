import pygame as pg
import random as rnd
import time
import os

home = os.path.expanduser("~")
dir = home + "\\AppData\\Local\\SnakeByFlo\\" #Speicherort des Highscores
filename = dir + "highscore.txt"

highscore = 0
if not os.path.exists(dir): #Wenn keine Path vorhanden dann erstellen und Highscore auf null stetzen
    os.makedirs(dir)
mode = 'r' if os.path.exists(filename) else 'w'
with open(filename, mode) as f:
    if mode == "r":
        highscore = round(float(f.readline()))
        f.close()



pg.init()
start = time.time()

BREITE, HÖHE = 1000, 600 #Daten von Schlange und Spiel
GRÖßE = 20
score, tempo = 0, 5
tempolimit = 23
appearBonus = 30
bonus_score = 10
snake = [(BREITE/2, HÖHE/2)]
richt_x,  richt_y = 1, 0
bonus_x, bonux_y = (rnd.randrange(BREITE))//GRÖßE * \
                    GRÖßE, (rnd.randrange(HÖHE)) // GRÖßE * GRÖßE  #Random Spawn von Bonus

screen = pg.display.set_mode([BREITE, HÖHE])
textfläche=pg.font.SysFont('impact', 28).render(
            f'Score: {round(score)}', True, (255, 255, 255))
highscoretext=pg.font.SysFont('impact', 28).render(
        f'Highscore: {round(highscore)}', True, (255, 255, 255))

while bonus_x + GRÖßE > BREITE - highscoretext.get_width() - 5 and bonux_y < highscoretext.get_height() + 5:
    bonus_x, bonux_y = (rnd.randrange(BREITE))//GRÖßE * \
                    GRÖßE, (rnd.randrange(HÖHE)) // GRÖßE * GRÖßE


bonustemp_x, bonustemp_y = (rnd.randrange(BREITE))//GRÖßE * \
                GRÖßE, (rnd.randrange(HÖHE)) // GRÖßE * GRÖßE
    
richtung = {pg.K_DOWN: (0, 1), pg.K_UP: (
    0, -1), pg.K_RIGHT: (1, 0), pg.K_LEFT: (-1, 0)}
richtung_u = {pg.K_DOWN: (0, 1), pg.K_RIGHT: (1, 0), pg.K_LEFT: (-1, 0)} 
richtung_d = {pg.K_UP: (0, -1), pg.K_RIGHT: (1, 0), pg.K_LEFT: (-1, 0)}
richtung_l = {pg.K_DOWN: (0, 1), pg.K_UP: (0, -1), pg.K_RIGHT: (1, 0)}
richtung_r = {pg.K_DOWN: (0, 1), pg.K_UP: (0, -1), pg.K_LEFT: (-1, 0)}

weiter = True
clock = pg.time.Clock()

nexttime = time.time()+0.5 #Delay für Ausgabe nach Spiel erstellen
nextbonus = time.time() + appearBonus
bonuscolor = False

while weiter:
    clock.tick(tempo)
    screen.fill((0, 0, 0))

    for ereignis in pg.event.get():
        if ereignis.type == pg.QUIT:
            weiter = False
        
        if len(snake) > 1: #Entgegengesetzer Richtungswechsel blockiert
            if ereignis.type == pg.KEYDOWN and ereignis.key in richtung_l and richt_x == 1 and richt_y == 0:
                richt_x, richt_y = richtung_l[ereignis.key]
            elif ereignis.type == pg.KEYDOWN and ereignis.key in richtung_r and richt_x == -1 and richt_y == 0:
                richt_x, richt_y = richtung_r[ereignis.key]
            elif ereignis.type == pg.KEYDOWN and ereignis.key in richtung_d and richt_x == 0 and richt_y == -1:
                richt_x, richt_y = richtung_d[ereignis.key]
            elif ereignis.type == pg.KEYDOWN and ereignis.key in richtung_u and richt_x == 0 and richt_y == 1:
                richt_x, richt_y = richtung_u[ereignis.key]
        else:
            if ereignis.type == pg.KEYDOWN and ereignis.key in richtung:
                richt_x, richt_y = richtung[ereignis.key]
        '''
            if ereignis.key == pg.K_UP:
                richt_x, richt_y = 0, -1
            if ereignis.key == pg.K_DOWN:
                richt_x, richt_y = 0, 1
            if ereignis.key == pg.K_RIGHT:
                richt_x, richt_y = 1, 0
            if ereignis.key == pg.K_LEFT:
                richt_x, richt_y = -1 , 0
        '''

    x, y = snake[-1]
    x, y = x + richt_x * GRÖßE, y + richt_y * GRÖßE #Größe und Richtung bestimmen

    if x < 0 or x + GRÖßE > BREITE or y < 0 or y + GRÖßE > HÖHE or (x, y) in snake: #Entscheiden wann das Spiel aus ist
        weiter = False
    snake.append((x, y)) #Falls man einen Bonus isst
        
    if x == bonus_x and y == bonux_y: #Bonus wächst exponential mit tempo, tempo ist aber gecapt
        score += tempo * bonus_score
        if tempo < tempolimit:
            tempo += rnd.random()*1.5+0.5
        bonus_x = rnd.randrange(BREITE) // GRÖßE * GRÖßE
        bonux_y = rnd.randrange(HÖHE) // GRÖßE * GRÖßE
        while bonus_x + GRÖßE > BREITE - highscoretext.get_width() - 5  and bonux_y < textfläche.get_height() + highscoretext.get_height() + 5 :
            bonus_x = rnd.randrange(BREITE) // GRÖßE * GRÖßE
            bonux_y = rnd.randrange(HÖHE) // GRÖßE * GRÖßE
    
    else: #Wenn nicht gegessen wird, bleibt man gleich und löscht den Eintrag
        del snake[0]
    
    for i in snake: #Snake wird absteigend dunkler
        pg.draw.rect(screen, (0, (30 + ((255-30) / (len(snake) - snake.index(i)))), 0), (i[0], i[1], GRÖßE, GRÖßE))

    if bonuscolor: #Blinkt hell/dunkel rot
        pg.draw.rect(screen, (255, 0, 0), (bonus_x, bonux_y, GRÖßE, GRÖßE))
        if time.time() - start > appearBonus:                        #Bonustemp erscheint erst nach 60 Sekunden
            pg.draw.rect(screen, (0, 0, 128), (bonustemp_x, bonustemp_y, GRÖßE, GRÖßE))
    else:
        pg.draw.rect(screen, (128, 0, 0), (bonus_x, bonux_y, GRÖßE, GRÖßE))
        if time.time() - start > appearBonus:
            pg.draw.rect(screen, (0, 0, 255), (bonustemp_x, bonustemp_y, GRÖßE, GRÖßE))

    if nexttime < time.time(): #Gleichbleibender Wechsel der Farben, sonst erhöht sich der Wechsel mit der Schleife
        bonuscolor=not bonuscolor
        nexttime=time.time() + 0.5

    if time.time() - start > appearBonus:
        if x == bonustemp_x and y == bonustemp_y: #Verringert tempo um die Hälfte und verringert Score
            tempo *= 0.75
            score *= 0.95

            appearBonus = time.time() - start + 30 #Tempobonus erscheint 30 Sekunden nach dem Aufnehmen von Bonus
            bonustemp_x = rnd.randrange(BREITE) // GRÖßE * GRÖßE
            bonuxtemp_y = rnd.randrange(HÖHE) // GRÖßE * GRÖßE

            #bonustemp_x = rnd.randrange(BREITE) // GRÖßE * GRÖßE
            #bonuxtemp_y = rnd.randrange(HÖHE) // GRÖßE * GRÖßE

    
    if score > highscore:
        textfläche=pg.font.SysFont('impact', 28).render(
        f'Score: {round(score)}', True, (255, 255, 0))
        screen.blit(textfläche, (BREITE - textfläche.get_width() - 5, 5)) #Score wird geld angezeigt wenn highscore gebrochen wird
    else:
        textfläche=pg.font.SysFont('impact', 28).render(
            f'Score: {round(score)}', True, (255, 255, 255))
        screen.blit(textfläche, (BREITE - textfläche.get_width() - 5, 5)) #Score oben rechts ausgeben

    highscoretext=pg.font.SysFont('impact', 28).render(
        f'Highscore: {round(highscore)}', True, (255, 255, 255))
    screen.blit(highscoretext, (BREITE - highscoretext.get_width() - 5, 50))

    pg.display.flip()
    

print("Spielzeit:", round((time.time() - start)//60),
      "Minute/n :", round((time.time() - start) % 60), "Sekunden") #Spielzeit in Minuten und Sekunden
print("Dein Score:", round(score))
if score > highscore: #Gib nur bei neuem Highscore aus
    print("Neuer Highscore:", round(score))

pg.quit()

mode = 'r' if os.path.exists(filename) else 'w'
        
with open(filename, mode) as f:  #File überschreiben wenn Score niedriger ist als Highscore, sonst Datei erstellen und Score eintragen
    if mode == "r":
        
        lines = f.readlines()

        if lines:
        
            if round(float(lines[0])) < round(score):
                with open(filename, 'w') as f:
                    f.write(str(round(score)))
                    f.close()
        else:
            with open(filename, 'a') as f:
                f.write(str(round(score)))
                f.close()
            
time.sleep(6) #delay das man die Ausgaben nach dem Spiel sehen kann, wenn man die Datei extern aufmacht